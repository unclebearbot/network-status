package org.bitbucket.unclebear.networkstatus;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class MainActivity extends AppCompatActivity {
    public static String LOCATION = "LOCATION";
    public static String STATUS_HANDSHAKE = "STATUS_HANDSHAKE";
    public static String STATUS_LATENCY = "STATUS_LATENCY";
    public static String STATUS_UNIT = "STATUS_UNIT";
    private RequestQueue requestQueue;

    @Override
    protected void onDestroy() {
        if (requestQueue != null) {
            requestQueue.cancelAll(Integer.MIN_VALUE);
        }
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        bind((ListView) findViewById(R.id.nodes), getNodes());
    }

    private void bind(ListView listView, Map<String, String> nodes) {
        List<Map<String, String>> results = new CopyOnWriteArrayList<>();
        final BaseAdapter adapter = bind(listView, results);
        requestQueue = Volley.newRequestQueue(this);
        for (Map.Entry<String, String> node : nodes.entrySet()) {
            Map<String, String> result = new HashMap<>();
            result.put(LOCATION, node.getKey());
            result.put(STATUS_HANDSHAKE, getResources().getString(R.string.status_init));
            result.remove(STATUS_LATENCY);
            result.remove(STATUS_UNIT);
            results.add(result);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
            queue(node, results, adapter);
        }
    }

    private BaseAdapter bind(ListView listView, List<Map<String, String>> results) {
        BaseAdapter adapter = new SimpleAdapter(
                this, results, R.layout.layout_node,
                new String[]{LOCATION, STATUS_HANDSHAKE, STATUS_LATENCY, STATUS_UNIT},
                new int[]{R.id.node_location, R.id.node_status_handshake, R.id.node_status_latency, R.id.node_status_unit}
        ) {
            @Override
            public boolean isEnabled(int position) {
                return false;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                updateColor(view);
                return view;
            }
        };
        listView.setAdapter(adapter);
        return adapter;
    }

    private void updateColor(View node) {
        if (node != null) {
            TextView latency = node.findViewById(R.id.node_status_latency);
            int color = getColor(String.valueOf(latency.getText()));
            latency.setTextColor(color);
            node.<TextView>findViewById(R.id.node_status_handshake).setTextColor(color);
            node.<TextView>findViewById(R.id.node_status_unit).setTextColor(color);
        }
    }

    private int getColor(String latencyString) {
        if (!latencyString.matches("^[0-9]+$")) {
            return 0xFFBDBDBD;
        }
        double latency = Double.parseDouble(latencyString);
        if (latency < 500) {
            return 0xFF1B5E20;
        }
        if (latency < 1000) {
            return 0xFFF57F17;
        }
        return 0xFFB71C1C;
    }

    private void queue(final Map.Entry<String, String> node, final List<Map<String, String>> results, final BaseAdapter adapter) {
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET, node.getValue(),
                getListener(node, results, adapter),
                getErrorListener(node, results, adapter)
        ) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String key = node.getKey();
                for (Map<String, String> result : results) {
                    if (key.equals(result.get(LOCATION))) {
                        result.put(STATUS_HANDSHAKE, getResources().getString(R.string.status_handshake));
                        result.put(STATUS_LATENCY, String.format(getResources().getString(R.string.status_latency), response.networkTimeMs));
                        result.put(STATUS_UNIT, getResources().getString(R.string.status_unit));
                        sort(results);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        });
                        break;
                    }
                }
                return super.parseNetworkResponse(response);
            }
        };
        stringRequest.setTag(Integer.MIN_VALUE);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1));
        if (requestQueue != null) {
            requestQueue.add(stringRequest);
        }
    }

    private Response.Listener<String> getListener(final Map.Entry<String, String> node, final List<Map<String, String>> results, final BaseAdapter adapter) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                queue(node, results, adapter);
            }
        };
    }

    private Response.ErrorListener getErrorListener(final Map.Entry<String, String> node, final List<Map<String, String>> results, final BaseAdapter adapter) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String key = node.getKey();
                for (Map<String, String> result : results) {
                    if (key.equals(result.get(LOCATION))) {
                        if (error != null && error.networkResponse != null) {
                            result.put(STATUS_HANDSHAKE, getResources().getString(R.string.status_handshake));
                            result.put(STATUS_LATENCY, String.format(getResources().getString(R.string.status_latency), error.getNetworkTimeMs()));
                            result.put(STATUS_UNIT, getResources().getString(R.string.status_unit));
                        } else {
                            result.put(STATUS_HANDSHAKE, getResources().getString(R.string.status_unreachable));
                            result.remove(STATUS_LATENCY);
                            result.remove(STATUS_UNIT);
                        }
                        sort(results);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        });
                        queue(node, results, adapter);
                        break;
                    }
                }
            }
        };
    }

    private void sort(List<Map<String, String>> results) {
        results.sort(new Comparator<Map<String, String>>() {
            @Override
            public int compare(Map<String, String> left, Map<String, String> right) {
                double leftLatency = Double.MAX_VALUE;
                if (left != null && left.containsKey(STATUS_LATENCY)) {
                    try {
                        leftLatency = Double.parseDouble(left.get(STATUS_LATENCY) + "");
                    } catch (Exception ignored) {
                    }
                }
                double rightLatency = Double.MAX_VALUE;
                if (right != null && right.containsKey(STATUS_LATENCY)) {
                    try {
                        rightLatency = Double.parseDouble(right.get(STATUS_LATENCY) + "");
                    } catch (Exception ignored) {
                    }
                }
                return Double.compare(leftLatency, rightLatency);
            }
        });
    }

    private Map<String, String> getNodes() {
        Map<String, String> nodes = new TreeMap<>();
        for (int i = R.string.node_01_location; i <= R.string.node_32_location; ++i) {
            nodes.put(getResources().getString(i), getResources().getString(++i));
        }
        return nodes;
    }
}
